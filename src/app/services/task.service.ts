import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/Operators';

import { TaskDetail } from 'src/app/models/task-detail';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TaskService {
  
  constructor(private http: Http) { }
  baseUrl: string = 'http://localhost/TaskManagerService/api/';

  // Get the all task list
  getTasks(): Observable<TaskDetail[]> {
    return this.http.get(this.baseUrl + 'GetAll')
      .pipe(map((response) => {
        return response.json();
      }) 
      );
  }

//create new task 
  addTask(task: any):Observable<any> {
    return this.http.post(this.baseUrl + 'AddTask', task)
      .pipe(map((response) => {
        return response;
      })
      );
  }
  //get task detail by task id
  getTaskById(id):Observable<TaskDetail> {
    return this.http.get(this.baseUrl + 'GetTask/' + id)
      .pipe(map((response) => {
        return response.json();
      })
      );
  }
// update the task details
  updateTask(task: any):Observable<any> {
    return this.http.put(this.baseUrl + 'Update', task)
      .pipe(map((response) => {
        return response;
      })
      );
  }
  //end the task 
  endTask(task:TaskDetail):Observable<TaskDetail[]> {
    return this.http.put(this.baseUrl + 'EndTask',task)
      .pipe(map((response) => {
        return response.json();
      })
      );
  }
  //delete the task
  deleteTask(id: string):Observable<TaskDetail[]> {
    return this.http.delete(this.baseUrl + 'DeleteTask/' + id)
      .pipe(map((response) => {
        return response.json();
      })
      );
  }

}
