export class TaskDetail {
    TaskId:string;
    TaskName:string;
    ParentTask:string;
    ParentId:string;
    Priority:number;
    StartDate:Date;
    EndDate:Date;
}


