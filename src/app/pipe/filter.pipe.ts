import { Pipe, PipeTransform } from '@angular/core';
import { TaskDetail } from 'src/app/models/task-detail';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  prioritysearchType: string
  transform(tasks: TaskDetail[], searchTask: string, searchParentTask: string, searchPriorityFrom: any,
    searchPriorityTo: any, searchStartDate: string, searchEndDate: string): TaskDetail[] {

      console.log("tasks", tasks);
    if (tasks && tasks.length) {
     
     // console.log("searchStartDate", searchStartDate);
     // console.log("searchEndDate", searchEndDate);


      return tasks.filter(task =>{
        if(searchTask && task.TaskName.toLocaleLowerCase().indexOf(searchTask.toLocaleLowerCase()) ===-1 ){
          return false;
        }
        if(searchParentTask && task.ParentTask && task.ParentTask.toLocaleLowerCase().indexOf(searchParentTask.toLocaleLowerCase()) ===-1 ){
          return false;
        }
        if(searchPriorityFrom && searchPriorityTo){
          if( task.Priority < searchPriorityFrom || task.Priority > searchPriorityTo)
          return false;
        }
        else if(searchPriorityFrom && task.Priority < searchPriorityFrom){
            return false;
        } 
        else if(searchPriorityTo && task.Priority > searchPriorityTo){
            return false;
        }
        if(searchStartDate && searchEndDate){
          if( task.StartDate.toString() < searchStartDate || task.StartDate.toString()  > searchEndDate)
          return false;
        }
        else if(searchStartDate && task.StartDate.toLocaleDateString() < searchStartDate){
            return false;
        } 
        else if(searchEndDate && task.EndDate.toString()  > searchEndDate){
            return false;
        }
         return true; 
      })
    }
    else {
         return tasks;
    }

  }

}
